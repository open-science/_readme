# WIN GitLab open science group

This group is a central point to create and share open science resources created using the WIN GitLab instance.

The group has currently has 4 subgroups:

- [analysis](https://git.fmrib.ox.ac.uk/open-science/analysis) - a place to share your analytic code or tools 
- [education](https://git.fmrib.ox.ac.uk/open-science/tasks) - a place to host your open educational materials
- [community](https://git.fmrib.ox.ac.uk/open-science/community) - a place to connect with the Open WIN Community and our activities
- [tasks](https://git.fmrib.ox.ac.uk/open-science/tasks) - a place to share your experimental task materials

Alongside these subgroups, you are also invited to share your materials directly from your own GitLab projects. We'd love it if such materials were indexed in the appropriate open-science subgroup, so they can be easily identified.
> To add your materials from your own or Lab's GitLab project, please add a link to it in the _README project of each subgroup listed above.

## Issues or suggestions

If you spot any mistakes in this documentation or would like to improve the guidelines, please let us know!

### New to GitLab
The easiest way to comment on this documentation is by submitting an "issue". Please review any existing issues for this project and select "New Issue" if you've spotted anything new!

Review or submit a new issue here: [https://git.fmrib.ox.ac.uk/open-science/_readme/-/issues](https://git.fmrib.ox.ac.uk/open-science/_readme/-/issues)

### Familiar with GitLab
If you are expereinced with using gitlab, please submit changes to thius project documentation following the fork and merge process. 

More training and guideance on this process will be made available soon.

## Contact

This project is maintained by the Open WIN Community. Please contact open@win.ox.ac.uk if you have any questions.




...
